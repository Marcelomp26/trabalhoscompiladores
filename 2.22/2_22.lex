%{
	/*	Programa lex para converter em caixa alta os comentarios da linguagem C.
		lex version 2.6.0
		Linux version 4.13.0-38-generic (buildd@lgw01-amd64-027) (gcc version 5.4.0 20160609 
		(Ubuntu 5.4.0-6ubuntu1~16.04.9)) #43~16.04.1-Ubuntu SMP Wed Mar 14 17:48:43 UTC 2018
	 */
	
	#include <stdio.h>
	#ifndef FALSE
	#define FALSE 0
	#endif
	#ifndef TRUE
	#define TRUE 1
	#endif
	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}

%% 

"/*" { 	char c;
		int done = FALSE;
		ECHO;
		do{	
			while((c=input())!='*')
				putchar(toupper(c));
			putchar(toupper(c));
			while((c=input())=='*')
				putchar(toupper(c));
			putchar(toupper(c));

			if(c == '/') done = TRUE;

		}while(!done);
	
}

%%

void main(void){
	yylex();
}



