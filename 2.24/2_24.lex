%{/*	Programa lex para conta o numero de linhas,caracteres e palavras da linguagem C.
		lex version 2.6.0
		Linux version 4.13.0-38-generic (buildd@lgw01-amd64-027) (gcc version 5.4.0 20160609 
		(Ubuntu 5.4.0-6ubuntu1~16.04.9)) #43~16.04.1-Ubuntu SMP Wed Mar 14 17:48:43 UTC 2018
	 */
	
	#include <stdio.h>
	#include <string.h>
	int count_char = 0;
	int count_word = 0;
	int count_line = 1;
	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}


digit [0-9]
number {digit}+
letter (([a-z])|([A-Z]))
character ({letter})|({digit})
word ({number}|{character}+|{number}{character}+|{character}+{number})
line "\n"

%%
{line} {count_line++;}

{word} {count_word++;
		count_char += strlen(yytext);} 

%%

main(void){
	yylex();
	fprintf (stderr, "number of character = %d\n", count_char);
	fprintf (stderr, "number of word = %d\n", count_word);
	fprintf (stderr, "number of line = %d\n", count_line);

	return 0;
}